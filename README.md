# Asteroids

written in Haskell

## DEPENDENCY GRAPH

```
          [Main]
          / | \
         /  |  \
   [Input]  |  [Draw]
         \  |  /
          \ | /
          [Game]
          / | \
         /  |  \
    [Ship]  |  [Asteroid]
         \  |  /
          \ | /
          [Body]
```

## TODO

### powerups

- Only one powerup at a time (f.e. after reaching n * 10000 points and no powerup yet present)
- keeps floating on screen, like asteroids, until caught. (possible to delay catching it)

**Which powerups??**

- laser
  - 10-ish seconds of full blast
  - drawn as thick line
  - actually implemented as constant high fire-rate.
- explosion
  - immediately explodes upon grabbing
  - hits all asteroids in vicinity & adds velocity

### sounds

- fire (subtle)
- hit (less subtle)
- thrusting (subtle)
- damage (less subtle)
- powerup (ka-ching!)

### refactoring

`pointInShape` from Math.hs to Asteroid.hs (probably rename)

### other

- UFO's !!
