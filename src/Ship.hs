{-# LANGUAGE TemplateHaskell #-}

module Ship where

import Control.Lens
import Graphics.Gloss.Interface.Pure.Game
import Foreign (fromBool)
import Data.List (sort)

import Body

data Ship = Ship {
  _body' :: Body,
  _thrusting :: Bool,
  _laser :: Int
}

makeLenses ''Ship
instance HasBody Ship where body = body'

initialShip :: Ship
initialShip = Ship (Body (0,0) (0,0) 0 0) False 0

stepShip :: Ship -> Ship
stepShip = donut 0 . drag 0.997 . stepBody . thrust

-- stepShip :: World -> World
-- stepShip = crash . over (ship.x) (donut 0) . "stepBody" . drag . thrusts . turn

-- turn :: Bool -> Bool -> Ship -> Ship
-- turn l r = over θ (+dθ)
--     where dθ = 0.05 * (fromBool r - fromBool l)

thrust :: Ship -> Ship
thrust ship | view thrusting ship = over v (+dv) ship
            | otherwise = ship
      where rot = view θ ship
            dv = 0.1 * (cos rot, -sin rot)

knockback :: Ship -> Ship
knockback ship = over v (+dv) ship
  where rot = view θ ship
        dv = -0.1 * (cos rot, -sin rot)

points :: Ship -> [(Float,Float)]
points s = map (view x s +)
  [ 20 * (cos θt, -sin θt)
  , c  * (cos θl, -sin θl)
  , 10 * (-cos θt, sin θt)
  , c  * (cos θr, -sin θr) ]
  where θt = view θ s
        θl = θt + atan2 1 (-2)
        θr = θt - atan2 1 (-2)
        c = 10 * (sqrt 5, sqrt 5)
