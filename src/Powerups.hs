{-# LANGUAGE TemplateHaskell #-}

module Powerups where

import System.Random.Stateful (Uniform (uniformM), uniformEnumM, StatefulGen, uniformRM)
import Control.Lens

import Body
import Math (tau)

data PowerupType = Laser
  -- | Explosion
  deriving (Enum, Bounded)

instance Uniform PowerupType where
  uniformM = uniformEnumM

data Powerup = Powerup {
    _power :: !PowerupType,
    _body' :: !Body
}

makeLenses ''Powerup
instance HasBody Powerup where body = body'

instance Uniform Powerup where
  uniformM g = do
    x <- randomBorder g
    φ <- uniformRM (0,tau) g
    let v = (cos φ * 0.4, -sin φ * 0.4)
    Powerup <$> uniformM g <*> pure (Body x v 0 0)
