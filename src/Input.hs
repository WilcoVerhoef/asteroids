module Input where

import Graphics.Gloss.Interface.Pure.Game
import Control.Lens

import Game
import Body
import Foreign (fromBool)
import Ship
import System.IO.Unsafe

rotateLeftKeys, rotateRightKeys, thrustKeys, doubleFireKeys, singleFireKeys, resetKeys :: [Key]
rotateLeftKeys  = [Char 'a', SpecialKey KeyLeft]
rotateRightKeys = [Char 'd', SpecialKey KeyRight]
thrustKeys      = [Char 'w', SpecialKey KeyUp]
doubleFireKeys  = [Char 'f', SpecialKey KeySpace]
singleFireKeys  = [MouseButton LeftButton, MouseButton RightButton]
resetKeys       = [Char 'r', Char 'R', Char '\DC2']

bound :: Float -> Float -> Float
bound b = max (-b) . min b

input :: Event -> World -> World
-- input (EventKey (Char 'r') Down (Modifiers Up Down Up) _) = const initial
input (EventKey key state _ _) | key `elem` doubleFireKeys = fire
                               | key `elem` singleFireKeys && state == Down = fire
                               | key `elem` rotateLeftKeys  = over (ship.ω) (bound 0.05 . flip (-) dω)
                               | key `elem` rotateRightKeys = over (ship.ω) (bound 0.05 . (+ dω))
                               | key `elem` thrustKeys = set (ship.thrusting) (state == Down)
    where dω | state == Down = 0.05
             | state == Up  = -0.05
input (EventKey key Down (Modifiers Up Down Up) _) | key `elem` resetKeys = const initial
input (EventKey (Char 'l') Down _ _) = over (ship.laser) (\i -> if i == 0 then 300 else 0)
input _ = id
