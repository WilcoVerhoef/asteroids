module Draw where

import Graphics.Gloss

import Control.Lens

import Game
import Math
import Body
import Asteroid
import Ship
import Powerups

draw :: [Int] -> World -> Picture
draw highs w = color yellow $ pictures
       [ hurt (view cd w),
         duplicate $ placeObject (view ship w) $ drawShip $ view (ship.thrusting) w,
         -- if view (ship.laser) w > 0 then
         --   placeObject (view ship w) $ line [(0,0), (width,0)]
         --   else blank,
         pictures (map asteroid $ view asteroids w),
         pictures (map (`placeObject` drawBullet) (view bullets w)),
         pictures (map (\p -> placeObject p (drawPowerup (view power p))) (view powerups w)),
         pictures $ take (view lives w) [translate (width/2 - o) (height/2 - 20) $ rotate 270 $ scale 0.5 0.5 (drawShip False) | o <- [20,40..]],
         translate (20 - width/2) (height/2 - 30) $ scale 0.2 0.2 $ text (show $ view score w),
         if view lives w == 0 then gameOver highs else blank,
         color (greyN 0.4) $ pictures [
           translate 0 height $ rectangleSolid (3 * width) height,
           translate 0 (-height) $ rectangleSolid (3 * width) height,
           translate width 0 $ rectangleSolid width height,
           translate (-width) 0 $ rectangleSolid width height
         ]
       ]
    where asteroid a = placeObject (view body a) (line (view poly a))

hurt :: Int -> Picture
hurt i = color (greyN (fromIntegral i / 60)) $ rectangleSolid width height

gameOver :: [Int] -> Picture
gameOver hs = pictures [
  translate (-314) (-180) $ scale 0.8 0.8 $ text "GAME OVER",
  translate (-288) (-280) $ scale 0.3 0.3 $ text "PRESS CTRL+R TO RESTART",
  translate 0 100 $ pictures [
    rectangleSolid 180 210,
    color black $ rectangleSolid 170 200,
    pictures [translate (-70) x $ scale 0.2 0.2 (text $ unwords [show i ++ ".", show h]) | (h,x,i) <- zip3 hs [35,5..] [1..]],
    translate (-74) 66 $ scale 0.19 0.25 $ text "HIGHSCORES"
    ]
  ]

duplicate :: Picture -> Picture
duplicate p = pictures [ translate (i * width) (j * height) p | i <- [-1..1], j <- [-1..1] ]

placeObject :: HasBody b => b -> Picture -> Picture
placeObject b = uncurry translate (view x b) . rotate (view θ b / pi * 180)

drawShip :: Bool -> Picture
drawShip True  = drawThrust <> drawShip False
drawShip False = scale 10 10 $ line [(2,0),(-2,1),(-1,0),(-2,-1),(2,0)]

drawThrust :: Picture
drawThrust = scale 10 10
           $ foldMap line
           [ [(-1.4-abs o,o),(-2.9-abs o,o)]
           | o <- [0.6,0.2,-0.2,-0.6] ]

drawBullet :: Picture
drawBullet = circleSolid 3

drawPowerup :: PowerupType -> Picture
drawPowerup Laser = scale 12 12 (starCoin 5 2)
-- drawPowerup Explosion = rectangleSolid 24 24

starCoin :: Int -> Int -> Picture
starCoin n m = scale (1/(1 + r/2)) (1/(1 + r/2))
             $ color black (circleSolid 1)
            <> thickCircle (1 + r/2) r
            <> pictures (take n $ iterate (rotate (360 / n')) oneArc)
  where oneArc = translate (-r) 0 $ thickArc (180-a/2) (180+a/2) 0.5 1
        -- the angle of an inner point
        a = 180 - 360*(m'-1)/n'
        -- the inner radius (origin ot inner point)
        r = cos (pi * m' / n') / cos (pi * (m'-1) / n')
        m' = fromIntegral m
        n' = fromIntegral n


-- -- |not working, only convex polygons work
-- starSolid :: Int -> Int -> Picture
-- starSolid n m = polygon $ take (2 * n) $ zipWith (\a d -> (d * cos a, d * sin a)) angles dists
--   where dists = cycle [1, cos (pi * m' / n') / cos (pi * (m'-1) / n')]
--         angles = [i * tau / 2 / n' | i <- [0..]]
--         m' = fromIntegral m
--         n' = fromIntegral n
