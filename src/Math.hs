{-# LANGUAGE FlexibleInstances #-}

module Math where

import Data.Fixed

width, height :: Float
width = 800
height = 600

tau :: Floating tau => tau
tau = 2 * pi

instance Num (Float,Float) where
    (x1,y1) + (x2,y2) = (x1 + x2, y1 + y2)
    (x1,y1) - (x2,y2) = (x1 - x2, y1 - y2)
    (x1,y1) * (x2,y2) = (x1 * x2, y1 * y2)
    abs (x,y) = (abs x, abs y)
    signum (x,y) = (signum x, signum y)
    fromInteger i = let n = fromInteger i in (n,n)

instance Fractional (Float,Float) where
    fromRational i = let n = fromRational i in (n,n)
    recip (x,y) = (recip x, recip y)

-- instance Floating (Float,Float) where
--     pi = (pi,pi)
--     exp (x,y) = (exp x, exp y)
--     log (x,y) = (log x, log y)


magnitude :: (Float,Float) -> Float
magnitude (x,y) = sqrt (x^2 + y^2)

-- todo optimize
pointInShape :: [Float] -> Float -> (Float,Float) -> (Float,Float) -> Bool
pointInShape sh θ pa pp = magnitude (rx,ry) < a + inter * (b-a)
    where (rx,ry) = pp - pa
          len = fromIntegral (length sh)
          dir = len * mod' ((- θ - atan2 ry rx) / tau) 1
          (part,inter) = properFraction dir
          sh' = last sh : sh
          (a,b) = (sh'!!part,sh'!!(part+1))
