{-# LANGUAGE TemplateHaskell #-}

module Asteroid where

import Control.Lens
import System.Random.Stateful
import Control.Monad

import Math
import Body

data Asteroid = Asteroid {
    _shape :: [Float],
    _poly :: [(Float,Float)],
    _size :: Int,
    _body' :: Body
}

makeLenses ''Asteroid
instance HasBody Asteroid where body = body'

initialAsteroids :: [Asteroid]
initialAsteroids = fst $ runStateGen (mkStdGen 1337) (replicateM 1 . randomAsteroid 3)

randomAsteroid :: StatefulGen g m => Int -> g -> m Asteroid
randomAsteroid i g = do

    x <- randomBorder g
    v <- randomVelocity i g
    ω <- uniformRM (-0.05, 0.05) g
    sh <- randomShape i g

    return $ Asteroid sh (shapeToPoly sh) i (Body x v 0 ω)

-- |split up into 3 smaller asteroids
explodeAsteroid :: StatefulGen g m => Asteroid -> g -> m [Asteroid]
explodeAsteroid a g
    | view size a <= 1 = return []
    | otherwise = replicateM 3 $ do

        let i = view size a - 1
        ω <- uniformRM (-0.05, 0.05) g
        v <- randomVelocity i g
        sh <- randomShape i g
        let x' = view x a + 5 * v

        return $ Asteroid sh (shapeToPoly sh) i (Body x' v 0 ω)

shapeToPoly :: [Float] -> [(Float,Float)]
shapeToPoly sh = zipWith fromPolar (last sh : sh) [0,tau / fromIntegral (length sh)..]
    where fromPolar ρ φ = (ρ * cos φ, -ρ * sin φ)

-- |random asteroid shape (of size i)
randomShape :: StatefulGen g m => Int -> g -> m [Float]
randomShape i = replicateM n . uniformRM r
    where n = [5,7,10] !! (i-1)
          r = [(5,15),(12,28),(30,50)] !! (i-1)

-- |random asteroid velocity (for asteroid of size i)
randomVelocity :: StatefulGen g m => Int -> g -> m (Float,Float)
randomVelocity i g = do
    φ <- uniformRM (0,tau) g
    ρ <- uniformRM (fromIntegral (4-i) * (0.2,0.8)) g
    return (cos φ * ρ, -sin φ * ρ)

pointInAsteroid :: Asteroid -> (Float,Float) -> Bool
pointInAsteroid = do
    sh <- view shape
    th <- view θ
    xx <- view x
    return (pointInShape sh th xx)
