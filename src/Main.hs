module Main where

import Graphics.Gloss

import Data.List
import Control.Lens
import Control.Monad
import Control.Monad.State.Lazy
import System.Directory
import Graphics.Gloss.Interface.IO.Game

import Game
import Draw
import Input
import Math
import Data.IORef

main :: IO ()
main = do

  highs <- readScores >>= newIORef

  let draw' w = do
        h <- readIORef highs
        return (draw h w)
  
  let input' e = return . input e
  
  let step' _ w = do
        let w' = step w
        let s = view score w'
        let dead = view lives w == 1 && view lives w' == 0
        when dead $ do 
          modifyIORef' highs (top5 . (s:))
          writeScore s
        return w'

  playIO (InWindow "Asteroids" (fromEnum width, fromEnum height) (300,300)) black 60 initial draw' input' step'

top5 :: [Int] -> [Int]
top5 = take 5 . sortOn negate . filter (/=0)

scoresFile :: IO FilePath
scoresFile = getAppUserDataDirectory "asteroids-highscores.txt"

readScores :: IO [Int]
readScores = do
  sf <- scoresFile
  appendFile sf ""
  top5 . map read . words <$> readFile sf

writeScore :: Int -> IO ()
writeScore s = do
  sf <- scoresFile
  appendFile sf (show s ++ "\n")
