{-# LANGUAGE TemplateHaskell #-}

module Game where

import Graphics.Gloss.Interface.Pure.Game

import Control.Lens
import System.Random
import Control.Monad.State.Strict

import Math
import Control.Monad
import System.Random.Stateful

import Asteroid
import Data.List (partition, sort)
import Body
import Ship
import Powerups

data World = World {
    _ship :: !Ship,
    _asteroids :: ![Asteroid],
    _bullets :: ![Body],
    _powerups :: ![Powerup],
    _score :: !Int,
    _level :: !Int,
    _lives :: !Int,
    _cd :: !Int,
    _gen :: !StdGen
}

makeLenses ''World

instance RandomGen World where
  split w = let (a,b) = split (view gen w) in (set gen a w, set gen b w)
  genWord32 w = let (a,b) = genWord32 (view gen w) in (a, set gen b w)

initial :: World
initial = World { _ship = initialShip
                , _asteroids = initialAsteroids
                , _bullets = []
                , _powerups = []
                , _score = 0
                , _level = 0
                , _lives = 5
                , _cd = 0
                , _gen = mkStdGen 1337 }

runRandomWorld :: (StateGenM World -> State World a) -> StateT World Identity a
runRandomWorld f = state (`runStateGen` f)

step :: World -> World
step w | view lives w > 0 = crash . powerup . over ship stepShip . stepPowerups . stepAsteroids . stepBullets $ w
       | view cd w > 0 = over cd (subtract 1) w
       | otherwise = w

crash :: World -> World
crash w | view cd w > 0 = over cd (subtract 1) w
        | null col = w
        | otherwise = flip execState w $ do
            -- todo mix code with stepBullet
            col' <- concat <$> mapM (runRandomWorld . explodeAsteroid) col
            let bonus = sum $ map (([0,110,70,30] !!) . view size) col
            modify (set asteroids (col' ++ nocol) . set cd 60 . over lives pred . over level (+bonus))
  where (col,nocol) = partition (\a -> any (pointInAsteroid a) (points $ view ship w)) (view asteroids w)

powerup :: World -> World
powerup w = flip execState w $ do
              laserTime <- gets $ view (ship.laser)
              when (laserTime > 0) $ modify (over (ship.laser) pred)
              when (odd laserTime) $ modify fire
              case ys of
                [] -> pure ()
                (y:ys) -> do
                  modify (set powerups (xs ++ ys))
                  case view power y of
                    Laser -> modify (set (ship.laser) 300)
                    -- Explosion -> undefined
  where (xs, ys) = break (\p -> any ((< 12) . magnitude . subtract (view (body.x) p)) (points $ view ship w)) (view powerups w)





-- ASTEROIDS!

stepAsteroids :: World -> World
stepAsteroids = over (asteroids . mapped) (donutA . stepBody)
  where donutA :: Asteroid -> Asteroid
        donutA a = donut ([0,30,56,100] !! view size a) a

summonAsteroid :: World -> World
summonAsteroid w = over asteroids (a:) . set gen g $ w
    where (a, g) = runStateGen (view gen w) (randomAsteroid 3)





-- POWERUPS!

stepPowerups :: World -> World
stepPowerups = over (powerups . mapped) (donut 32 . stepBody)

summonPowerup :: World -> World
summonPowerup w = over powerups (p:) . set gen g $ w
    where (p, g) = uniform (view gen w)





-- BULLETS!

fire :: World -> World
fire w = over ship knockback $ over bullets (b:) w
    where b = Body (view (ship.x) w) (view (ship.v) w + v') 0 0
          v' = (10,10) * (cos rot, -sin rot)
          rot = view (ship.θ) w


stepBullets :: World -> World
stepBullets = execState collideBullets . over bullets (filter (inside . view x)) . over (bullets.mapped) stepBody
  where inside (x,y) = abs x < width / 2 && abs y < height / 2

collideBullets :: State World ()
collideBullets = do
  bs <- gets (view bullets)
  bs' <- filterM stepBullet bs
  modify (set bullets bs')

stepBullet :: Body -> State World Bool
stepBullet b = do
  as <- gets (view asteroids)

  let (i,(a,j)) = splitAt 1 <$> break hit as
  
  case a of
    [] -> return True
    [a] -> do
      as' <- state $ flip runStateGen (explodeAsteroid a)
      modify (set asteroids (i ++ as' ++ j))

      -- update score
      let bonus = [0,110,70,30] !! view size a
      
      -- summon new asteroid
      s <- gets (view level)
      when (mod s 1_000 + bonus >= 1_000 || null as) (modify summonAsteroid)
      
      -- summon powerup
      when (mod s 10_000 + bonus >= 10_000) (modify summonPowerup)

      modify (over score (+ bonus) . over level (+ bonus))

      return False

  where hit a = pointInAsteroid a (view x b)
