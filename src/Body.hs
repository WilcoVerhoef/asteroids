{-# LANGUAGE TemplateHaskell #-}

module Body where

import Control.Lens (view, over,  makeClassy)

import Math ( width, height )
import System.Random.Stateful

-- |a moving/rotating shape
data Body = Body {
    _x :: (Float,Float),
    _v :: (Float,Float),
    _θ :: Float,
    _ω :: Float
}

makeClassy ''Body

-- |random position along a border
randomBorder :: StatefulGen g m => g -> m (Float,Float)
randomBorder = fmap f . uniformRM (0,width + height)
    where f p | p < width = (p,height/2 + 50)
              | otherwise = (width/2 + 50,p)

drag :: HasBody b => Float -> b -> b
drag x = over v (*(x,x))

stepBody :: HasBody b => b -> b
stepBody b = over x (+dx) . over θ (+dθ) $ b
    where dx = view v b
          dθ = view ω b

donut :: HasBody b => Float -> b -> b
donut i = over x f
    where f (x,y) | x >  w / 2 = f (x-w, y)
                  | x < -w / 2 = f (x+w, y)
                  | y >  h / 2 = f (x, y-h)
                  | y < -h / 2 = f (x, y+h)
                  | otherwise = (x,y)
          w = width + i
          h = height + i
